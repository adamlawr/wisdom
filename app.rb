require 'logger'

module Nesta

  class App
    GIT_REVISION = "#{`cat .git-rev`}".freeze
    ENV_DEETS = "env: #{ENV['RACK_ENV']}, git: #{GIT_REVISION}, who: #{`whoami`}, dir: #{`pwd`}".freeze

    def logger
      @logger ||= Logger.new("#{settings.root}/log/#{settings.environment}.log")
    end

    get '/' do
      set_common_variables
      @page = Nesta::Page.find_by_path('index')
      haml(:index, layout: :layout)
    end

    error do
      env['sinatra.error'].backtrace
    end

    helpers do
      def index_by_category
        haml_tag :ul, class: 'top' do
          all_categories.each do |category|
            haml_tag :li do
              haml_concat category
              haml_tag :span, :<, class: "toggler icon-plus"
              haml_tag :ul, class: 'category collapsed' do
                sorted_pages_for_category(category).each do |page|
                  haml_tag :li do
                    haml_tag :a, :<, href: path_to(page.abspath) do
                      haml_concat page.link_text
                    end
                  end
                end
              end
            end
          end
        end
      end

      def all_categories
        categories = []
        Page.find_all.each do |page|
          categories << categories_for_page(page)
        end
        categories.flatten.uniq.sort
      end

      def categories_for_page(page)
        page_categories = page.metadata('categories')
        return [] if page_categories.nil?
        page_categories.split(',').map { |cat| cat.strip }
      end

      def pages_for_category(category)
        Page.find_all.select do |page|
          categories_for_page(page).include?(category)
        end
      end

      def sorted_pages_for_category(category)
        pages_for_category(category).sort
      end
    end
  end

  class Page
    def to_s
      "path: #{path}, #{'parent: ' + parent.title if parent}, title: #{title}, categories: #{metadata('categories')}"
    end

    def <=>(other)
      title <=> other.title
    end
  end
end
