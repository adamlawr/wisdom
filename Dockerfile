FROM ruby:2.6.5-alpine
RUN apk update && apk add build-base libxml2 libgcrypt-dev git
RUN mkdir /wisdom
WORKDIR /wisdom
COPY Gemfile /wisdom/Gemfile
COPY Gemfile.lock /wisdom/Gemfile.lock
RUN gem install bundler
RUN bundle install
COPY . /wisdom

ENTRYPOINT ["/usr/local/bundle/bin/rackup"]
CMD ["--host", "0.0.0.0", "-p", "3000"]
