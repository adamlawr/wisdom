# raceweb wisdom

personal documentation site

## links
- [nesta cms](http://nestacms.com/)
- [haml reference](http://haml.info/docs/yardoc/file.REFERENCE.html)
- [toc plugin](https://gitlab.com/adamlawr/nesta-plugin-toc)
- [view count plugin](https://gitlab.com/adamlawr/nesta-plugin-view-count)

## plugins
### table of contents
generates a table of contents for a page based on nested header tags in the content  
[toc plugin](https://gitlab.com/adamlawr/nesta-plugin-toc)

### view count data
maintains view count and last viewed date in metadata  
[view count plugin](https://gitlab.com/adamlawr/nesta-plugin-view-count)

## run it
### local

```bash
$ bundle exec rackup config.ru
```

### local with docker

```bash
$ docker build -t wisdom-web .
$ docker run -d -v ~/Development/nesta/wisdom-content:/wisdom-content -p 3001:3001 --name ww wisdom-web
```

### on a vps

1. create docker image and push it to [gitlab](https://gitlab.com/adamlawr/wisdom)

```bash
$ docker login registry.gitlab.com
$ docker build -t registry.gitlab.com/adamlawr/wisdom:20200603 .
$ docker push registry.gitlab.com/adamlawr/wisdom:20200603
```

2. run it from the server

```bash
$ ssh vultr1
$ docker run -d -v /wisdom-content:/wisdom-content -p 3001:3001 --name wisdom_web registry.gitlab.com/adamlawr/wisdom:20200603
```

## get content to the vps

1. make an ssh alias for the server
edit `~/.ssh/config` with a section like this:

```
Host linode
    Hostname 45.56.87.13
    User root
    IdentityFile /Users/adam/.ssh/linode_rsa
```

2. make a keypair and upload the public key

```bash
$ ssh-keygen
...
$ ssh-copy-id -i ~/.ssh/linode_rsa root@173.255.242.149
```

3. create a bare repository for the content and a target directory for the hook to check out content to.

```bash
$ ssh linode
$ git init --bare /wisdom-content.git
$ mkdir /wisdom-content
```

4. create a post-recieve hook to checkout the content

```bash
# /wisdom-content.git/hooks/post-receive

#!/bin/sh

while read oldrev newrev refname
do
    # Get the name of the current branch
    branch=$(git rev-parse --symbolic --abbrev-ref $refname)

    # Checkout master branch
    if [ "$branch" = "master" ]; then
      git --work-tree=/wisdom-content --git-dir=/wisdom-content.git checkout -f master
    fi
done
```

5. make it executable

```bash
    $ chmod +x /wisdom-content.git/hooks/post-receive
```

## create and update content

1. add a git remote for the bare repo on the server

```bash
$ git remote add linode ssh://linode/wisdom-content.git
```

2. use a text editor to edit markdown content in a local repo
3. push content to the server with git

```bash
$ git push linode
```
